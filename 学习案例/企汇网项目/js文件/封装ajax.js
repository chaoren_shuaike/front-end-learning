function ajax(options) {
    var xhr = new XMLHttpRequest();
    var data = options.data;
    if (options.methods == 'GET') {
        xhr.open(options.methods, options.url);
        xhr.send();
    } else if (options.methods == 'POST') {
        xhr.open(options.methods, options.url);
        // 如果想要使用post提交数据,必须添加此行
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        // 将数据通过send方法传递
        xhr.send(data);
    }
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            options.success(JSON.parse(xhr.responseText));
        } 
        // else {
        //     options.defeat();
        // }
    };
}
