/*
    ajax({
        url:"",
        data:{
            name:"张三",
            age:20
        },
        methods:"",不传 默认就是 get
        success:function(res){
            
        }
    })
*/
// http://127.0.0.1:9527/api/goodList?page=1&type=2
function ajax(obj) {
    // 处理有无参数  有参数的时候  拼接字符串
    //page=1&type=2

    var queryStr = '';

    if (obj.data) {
        for (var key in obj.data) {
            queryStr += `${key}=${obj.data[key]}&`;
        }
    }
    queryStr = queryStr.replace(/&$/, '');

    // 处理请求方式
    var type = obj.type || 'GET';

    // 处理请求路径
    var url = obj.url;
    if (type == 'GET' && queryStr != '') {
        url += '?' + queryStr;
    }

    var xhr = new XMLHttpRequest();

    xhr.open(type, url);

    if (type == 'GET') {
        xhr.send();
    } else {
        // post 需要设置请求头
        xhr.setRequestHeader('content-type', 'application/x-www-form-urlencoded');
        // 把参数 放到 send 里面
        xhr.send(queryStr);
    }

    xhr.onreadystatechange = function () {
        if (xhr.status == 200 && xhr.readyState == 4) {
            obj.success(xhr.responseText);
        }
    };
}
