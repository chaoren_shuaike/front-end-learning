$(function () {
    $('.topTel').hover(function () {
        $('.dropDownLayer').stop().slideToggle();
    });
    $('.dropDown_soucang').hover(function () {
        $('.dropDown_on_soucang').stop().slideToggle();
    });
    $('.dropDown_caigou').hover(function () {
        $('.dropDown_on_caigou').stop().slideToggle();
    });
    $('.dropDown_gongying').hover(function () {
        $('.dropDown_on_gongying').stop().slideToggle();
    });
    $('.dropDown_qihui').hover(function () {
        $('.dropDown_on_qihui').stop().slideToggle();
    });
    $('.dropDown_kefu').hover(function () {
        $('.dropDown_on_kefu').stop().slideToggle();
    });
    $('.dropDown_daohang').hover(function () {
        $('.dropDown_on_daohang').stop().slideToggle();
    });

    $('.nav a').mouseenter(function () {
        $(this).addClass('navActive');
    });
    $('.nav a').mouseleave(function () {
        if ($(this).attr('class') != 'nav_active navActive') {
            $(this).removeClass('navActive');
        }
    });
    var qgjAboutH2Top = $('.qgjAbout h2').offset().top;
    var qgjAdvantageH2Top = $('.qgjAdvantage h2').offset().top;
    var qgjCaseH2Top = $('.qgjCase h2').offset().top;
    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
        var innerHeight = $(window).innerHeight() / 2;
        // console.log(scrollTop, innerHeight);
        if (qgjAboutH2Top - 100 < scrollTop + innerHeight) {
            $('.qgjAbout h2').animate({
                fontSize: 32 + 'px',
                opacity: 1,
            });
            $('.qgjAbout .p').each(function (i, p) {
                $(p).animate({
                    marginTop: 0 + 'px',
                    opacity: 1,
                });
            });
        }
        if (qgjAdvantageH2Top - 50 < scrollTop + innerHeight) {
            $('.qgjUl').animate({
                marginTop: 40 + 'px',
            });
        }
        if (qgjCaseH2Top - 50 < scrollTop + innerHeight) {
            $('.qgjCase h2').animate({
                fontSize: 32 + 'px',
                opacity: 1,
            });
            $('.qgjCase .qCaseul .qCaseul_a').animate(
                {
                    right: 0 + 'px',
                    opacity: 1,
                },
                1000
            );
            $('.qgjCase .qCaseul .qCaseul_b').animate(
                {
                    left: 0 + 'px',
                    opacity: 1,
                },
                1000
            );
        }
    });
    var prevCircleIndex = 0;
    $('.circle li').click(function (e) {
        var imgWidth = $('.main').width();
        var circleIndex = $(e.target).index();
        $('.list')
            .stop()
            .animate(
                {
                    left: -imgWidth * circleIndex + 'px',
                },
                1000
            );
        $('.circle').children().eq(prevCircleIndex).removeClass('current');
        $('.circle').children().eq(circleIndex).addClass('current');
        prevCircleIndex = circleIndex;
    });
    var flag = true;
    var timer;
    timer = setInterval(function () {
        if (flag) {
            $('.circle').children().eq(1).click();
            flag = false;
        } else {
            $('.circle').children().eq(0).click();
            flag = true;
        }
    }, 6000);
    $('.main').mouseenter(function () {
        clearInterval(timer);
        timer = null;
    });
    $('.main').mouseleave(function () {
        timer = setInterval(function () {
            if (flag) {
                $('.circle').children().eq(1).click();
                flag = false;
            } else {
                $('.circle').children().eq(0).click();
                flag = true;
            }
        }, 6000);
    });
    $('.qgjUl li').each(function (i, item) {
        $(item).mouseenter(function () {
            $(item).children().eq(3).stop().animate({display: 'block', opacity: 1});
            $(item).stop().animate({
                marginTop: '-15px',
            });
        });
        $(item).mouseleave(function () {
            $(item).children().eq(3).stop().animate({display: 'none', opacity: 0});
            $(item).stop().animate({
                marginTop: '0px',
            });
        });
    });
    $('.qiHui .qiHuiBtn').mouseenter(function (e) {
        $(e.target).stop().animate({
            backgroundPosition: '142px',
        });
    });
    $('.qiHui .qiHuiBtn').mouseleave(function (e) {
        $(e.target).stop().animate({
            backgroundPosition: '135px',
        });
    });
    $('.qgjCase .qgjCaseAnLi .qgjCaseBtn').mouseenter(function (e) {
        $(e.target).stop().animate({
            backgroundPosition: '142px',
        });
    });
    $('.qgjCase .qgjCaseAnLi .qgjCaseBtn').mouseleave(function (e) {
        $(e.target).stop().animate({
            backgroundPosition: '135px',
        });
    });
    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 300) {
            $('#backTop').css({
                display: 'block',
            });
        } else {
            $('#backTop').css({
                display: 'none',
            });
        }
    });
    $('#backTop').click(function () {
        var timer = setInterval(function () {
            var scrollTop = $(window).scrollTop();
            var speedTop = scrollTop / 5;
            if (scrollTop <= 0) {
                clearInterval(timer);
                scrollTop = 0;
            }
            $('html,body').scrollTop(scrollTop - speedTop);
        }, 20);
    });
});
