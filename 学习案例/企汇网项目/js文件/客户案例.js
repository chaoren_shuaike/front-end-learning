$(function () {
    $('.topTel').hover(function () {
        $('.dropDownLayer').stop().slideToggle();
    });
    $('.dropDown_soucang').hover(function () {
        $('.dropDown_on_soucang').stop().slideToggle();
    });
    $('.dropDown_caigou').hover(function () {
        $('.dropDown_on_caigou').stop().slideToggle();
    });
    $('.dropDown_gongying').hover(function () {
        $('.dropDown_on_gongying').stop().slideToggle();
    });
    $('.dropDown_qihui').hover(function () {
        $('.dropDown_on_qihui').stop().slideToggle();
    });
    $('.dropDown_kefu').hover(function () {
        $('.dropDown_on_kefu').stop().slideToggle();
    });
    $('.dropDown_daohang').hover(function () {
        $('.dropDown_on_daohang').stop().slideToggle();
    });
    $('.nav a').mouseenter(function () {
        $(this).addClass('navActive');
    });
    $('.nav a').mouseleave(function () {
        if ($(this).attr('class') != 'nav_active navActive') {
            $(this).removeClass('navActive');
        }
    });
    var prevCircleIndex = 0;
    $('.circle li').click(function (e) {
        var imgWidth = $('.main').width();
        var circleIndex = $(e.target).index();
        $('.list')
            .stop()
            .animate(
                {
                    left: -imgWidth * circleIndex + 'px',
                },
                1000
            );
        $('.circle').children().eq(prevCircleIndex).removeClass('current');
        $('.circle').children().eq(circleIndex).addClass('current');
        prevCircleIndex = circleIndex;
    });
    var flag = true;
    var timer;
    timer = setInterval(function () {
        if (flag) {
            $('.circle').children().eq(1).click();
            flag = false;
        } else {
            $('.circle').children().eq(0).click();
            flag = true;
        }
    }, 6000);
    $('.main').mouseenter(function () {
        clearInterval(timer);
        timer = null;
    });
    $('.main').mouseleave(function () {
        timer = setInterval(function () {
            if (flag) {
                $('.circle').children().eq(1).click();
                flag = false;
            } else {
                $('.circle').children().eq(0).click();
                flag = true;
            }
        }, 6000);
    });

    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 300) {
            $('#backTop').css({
                display: 'block',
            });
        } else {
            $('#backTop').css({
                display: 'none',
            });
        }
    });
    $('#backTop').click(function () {
        var timer = setInterval(function () {
            var scrollTop = $(window).scrollTop();
            var speedTop = scrollTop / 5;
            if (scrollTop <= 0) {
                clearInterval(timer);
                scrollTop = 0;
            }
            $('html,body').scrollTop(scrollTop - speedTop);
        }, 20);
    });

    // ajax({
    //     url: 'http://192.168.107.86:9527/api/goodList?page=1',
    //     data: {
    //         name: '张三',
    //         age: 18,
    //     },
    //     methods: 'GET',
    //     success: function (res) {
    //         randomTable(res);
    //     },
    //     defeat: function () {
    //         randomTable2();
    //     },
    // });
    var baseUrl = 'http://192.168.107.53:9527/api';
    var obj = {};
    ajax({
        url: baseUrl + '/getTypeone',
        success: function (res) {
            console.log(JSON.parse(res));
            var arr = JSON.parse(res);

            arr.forEach(function (item, index, arr) {
                ajax({
                    url: baseUrl + '/goodList',
                    data: {
                        page: 1,
                        type_one: item,
                    },
                    success: function (res2) {
                        // console.log(JSON.parse(res2));
                        obj[item] = JSON.parse(res2);

                        Object.keys(obj).length == arr.length ? fun() : '';
                    },
                });
            });
        },
    });
    function fun() {
        var dataArr = [];
        // console.log(obj);
        for (var key in obj) {
            // console.log(res[key]);
            dataArr.push(obj[key]);
        }
        // console.log(dataArr);
        var newDataArr = dataArr.slice(11, 19);
        // dataArr.forEach(function (item, i) {
        //     if (i > 11 && i < 20) {
        //         newDataArr.push(item);
        //     }
        // });
        console.log(newDataArr);
        for (var i = 0; i < newDataArr.length; i++) {
            randomTable(newDataArr[i], i);
        }
        // var tbodyStr = '';
        // for (var i = 0; i < res.length; i++) {
        //     tbodyStr += `
        //     <tr>
        //         <td>${res[i].Id}</td>
        //         <td>${res[i].title}</td>
        //         <td>
        //             <img class="myImg"
        //                 src="${i < 2 ? res[i].imageUrl : '../images/lazyload.gif'}"
        //                 alt=""
        //             />
        //         </td>
        //         <td>${res[i].priceStr}</td>
        //     </tr>`;
        //     imgArr.push(res[i].imageUrl);
        // }
        // imgArr.length == res.length ? fun() : '';
        // $('.caseBox .caseBoxRi .tableData tbody').each(function (i, tbody) {
        //     $(tbody).html(tbodyStr);
        // });
    }
    var allImgArr = [];
    function randomTable(res, index) {
        var tbodyStr = '';
        var imgArr = [];
        for (var i = 0; i < res.length; i++) {
            tbodyStr += `
            <tr>
                <td>${res[i].Id}</td>
                <td>${res[i].title}</td>
                <td>
                    <img class="myImg"
                        src="${i < 2 ? res[i].imageUrl : '../images/lazyload.gif'}"
                        alt=""
                    />
                </td>
                <td>${res[i].priceStr}</td>
            </tr>`;
            imgArr.push(res[i].imageUrl);
        }
        allImgArr.push(imgArr);
        allImgArr.length == 8 ? lazyLoad(allImgArr) : '';
        $($('.caseBox .caseBoxRi .tableData tbody')[index]).html(tbodyStr);
        // console.log(tbodyStr);
        // imgArr.length == res.length ? fun() : '';
        // $('.caseBox .caseBoxRi .tableData tbody').each(function (i, tbody) {
        //     $(tbody).html(tbodyStr);
        // });
    }
    // var oldImgArr = Array.from(document.getElementsByClassName('myImg'));
    // console.log(oldImgArr);
    var imgArr2 = [];
    var sliceImgArr2 = [];
    function lazyLoad(allImgArr) {
        setTimeout(function () {
            var sliceImgArr = [];
            var oldImgArr = Array.from(document.getElementsByClassName('myImg'));
            var arrLength = oldImgArr.length;
            var num = 30;
            for (var i = 0; i <= arrLength; i++) {
                if (i % num == 0 && i != 0) {
                    sliceImgArr.push(oldImgArr.slice(i - num, i));
                }
            }
            console.log(sliceImgArr, allImgArr);
            imgArr2 = allImgArr;
            sliceImgArr2 = sliceImgArr;
        }, 1200);
    }
    // var imgArr2 = [];
    // function randomTable2() {
    //     var tbodyStr = '';
    //     for (var i = 0; i < goodList.length; i++) {
    //         tbodyStr += `
    //         <tr>
    //             <td>${goodList[i].Id}</td>
    //             <td>${goodList[i].title}</td>
    //             <td>
    //                 <img class="myImg"
    //                     src="../images/lazyload.gif"
    //                     alt=""
    //                 />
    //             </td>
    //             <td>${goodList[i].priceStr}</td>
    //         </tr>`;
    //         imgArr2.push(goodList[i].imageUrl);
    //     }
    //     imgArr2.length == goodList.length ? fun() : '';
    //     $('.caseBox .caseBoxRi .tableData tbody').each(function (i, tbody) {
    //         $(tbody).html(tbodyStr);
    //     });
    // }
    // var imgArr2 = [];
    // var sliceImgArr2 = [];
    // function fun() {
    //     // randomTable(obj);
    //     setTimeout(function () {
    //         var sliceImgArr = [];
    //         var oldImgArr = Array.from(document.getElementsByClassName('myImg'));
    //         var arrLength = oldImgArr.length;
    //         var num = 30;
    //         for (var i = 0; i <= arrLength; i++) {
    //             if (i % num == 0 && i != 0) {
    //                 sliceImgArr.push(oldImgArr.slice(i - num, i));
    //             }
    //         }
    //         imgArr2 = imgArr;
    //         sliceImgArr2 = sliceImgArr;
    //     }, 1000);
    // }
    var tableData = document.querySelectorAll('.tableData');
    tableData.forEach(function (table, i) {
        table.addEventListener('scroll', function () {
            // console.log(imgArr2[i], sliceImgArr2[i]);
            var innerHeight = $($('.tableData')).height() / 2;
            var scrollTop = $(this).scrollTop();
            var tbodyTop = $($(this).children()[1]).offset().top;
            sliceImgArr2[i].forEach(function (item, index) {
                var ot = $(item).offset().top - tbodyTop;
                if (ot - 30 <= scrollTop + innerHeight) {
                    item.src = imgArr2[i][index];
                }
            });
        });
    });
});
