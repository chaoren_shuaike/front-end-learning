$(function () {
    $('.topTel').hover(function () {
        $('.dropDownLayer').stop().slideToggle();
    });
    $('.dropDown_soucang').hover(function () {
        $('.dropDown_on_soucang').stop().slideToggle();
    });
    $('.dropDown_caigou').hover(function () {
        $('.dropDown_on_caigou').stop().slideToggle();
    });
    $('.dropDown_gongying').hover(function () {
        $('.dropDown_on_gongying').stop().slideToggle();
    });
    $('.dropDown_qihui').hover(function () {
        $('.dropDown_on_qihui').stop().slideToggle();
    });
    $('.dropDown_kefu').hover(function () {
        $('.dropDown_on_kefu').stop().slideToggle();
    });
    $('.dropDown_daohang').hover(function () {
        $('.dropDown_on_daohang').stop().slideToggle();
    });
    $('.nav a').mouseenter(function () {
        $(this).addClass('navActive');
    });
    $('.nav a').mouseleave(function () {
        if ($(this).attr('class') != 'nav_active navActive') {
            $(this).removeClass('navActive');
        }
    });
    var prevCircleIndex = 0;
    $('.circle li').click(function (e) {
        var imgWidth = $('.main').width();
        var circleIndex = $(e.target).index();
        $('.list')
            .stop()
            .animate(
                {
                    left: -imgWidth * circleIndex + 'px',
                },
                1000
            );
        $('.circle').children().eq(prevCircleIndex).removeClass('current');
        $('.circle').children().eq(circleIndex).addClass('current');
        prevCircleIndex = circleIndex;
    });
    var flag = true;
    var timer;
    timer = setInterval(function () {
        if (flag) {
            $('.circle').children().eq(1).click();
            flag = false;
        } else {
            $('.circle').children().eq(0).click();
            flag = true;
        }
    }, 6000);
    $('.main').mouseenter(function () {
        clearInterval(timer);
        timer = null;
    });
    $('.main').mouseleave(function () {
        timer = setInterval(function () {
            if (flag) {
                $('.circle').children().eq(1).click();
                flag = false;
            } else {
                $('.circle').children().eq(0).click();
                flag = true;
            }
        }, 6000);
    });
    var posTableTop = $('.postTable').offset().top;
    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > 300) {
            $('#backTop').css({
                display: 'block',
            });
        } else {
            $('#backTop').css({
                display: 'none',
            });
        }
        if (scrollTop >= posTableTop) {
            $('.postTable').addClass('fixed');
        } else {
            $('.postTable').removeClass('fixed');
        }
        if ($(window).scrollTop() > 850) {
            $('.caseBoxbot').addClass('fixed2');
        } else {
            $('.caseBoxbot').removeClass('fixed2');
        }
        var scrollBottom = $(document).height() - $(window).height() - $(window).scrollTop();
        if (scrollBottom <= 685) {
            $('.caseBoxbot').removeClass('fixed2');
        }
    });
    $('#backTop').click(function () {
        var timer = setInterval(function () {
            var scrollTop = $(window).scrollTop();
            var speedTop = scrollTop / 5;
            if (scrollTop <= 0) {
                clearInterval(timer);
                scrollTop = 0;
            }
            $('html,body').scrollTop(scrollTop - speedTop);
        }, 20);
    });
    ajax({
        url: 'http://192.168.107.53:9527/api/goodList?page=1',
        data: {
            name: '张三',
            age: 18,
        },
        methods: 'GET',
        success: function (res) {
            randomTable(res);
        },
    });
    var imgArr = [];
    function randomTable(res) {
        var tbodyStr = '';
        for (var i = 0; i < res.length; i++) {
            tbodyStr += ` 
            <tr>
                <td>${res[i].Id}</td>
                <td>${res[i].title}</td>
                <td>
                    <img class="myImg"
                        src="../images/lazyload.gif"
                        alt=""
                    />
                </td>
                <td>${res[i].priceStr}</td>
            </tr>`;
            imgArr.push(res[i].imageUrl);
        }
        imgArr.length == res.length ? fun() : '';
        $('.postBox .tableData tbody').html(tbodyStr);
    }

    function fun() {
        setTimeout(function () {
            var imgs = Array.from(document.querySelectorAll('.myImg'));
            // console.log(imgs, imgArr);
            // 窗口高度的一半
            $(window).scroll(function () {
                var innerHeight = $(window).innerHeight() / 2;
                // 滚动高度
                var scrollTop = $(window).scrollTop();
                for (var i = 0; i < imgs.length; i++) {
                    // 获取当前img的offsetTop值
                    var ot = $(imgs).eq(i).offset().top;
                    // 当当前的offsetTop值减70小于等于滚动高度加窗口高度的一半
                    if (ot - 150 <= scrollTop + innerHeight) {
                        // 替换为真实的src地址
                        imgs[i].src = imgArr[i];
                    }
                }
            });
        }, 500);
    }
});
