$(function () {
    $('.topTel').hover(function () {
        $('.dropDownLayer').stop().slideToggle();
    });
    $('.dropDown_soucang').hover(function () {
        $('.dropDown_on_soucang').stop().slideToggle();
    });
    $('.dropDown_caigou').hover(function () {
        $('.dropDown_on_caigou').stop().slideToggle();
    });
    $('.dropDown_gongying').hover(function () {
        $('.dropDown_on_gongying').stop().slideToggle();
    });
    $('.dropDown_qihui').hover(function () {
        $('.dropDown_on_qihui').stop().slideToggle();
    });
    $('.dropDown_kefu').hover(function () {
        $('.dropDown_on_kefu').stop().slideToggle();
    });
    $('.dropDown_daohang').hover(function () {
        $('.dropDown_on_daohang').stop().slideToggle();
    });
    $('.nav a').mouseenter(function () {
        $(this).addClass('navActive');
    });
    $('.nav a').mouseleave(function () {
        if ($(this).attr('class') != 'nav_active navActive') {
            $(this).removeClass('navActive');
        }
    });
    var prevCircleIndex = 0;
    $('.circle li').click(function (e) {
        var imgWidth = $('.main').width();
        var circleIndex = $(e.target).index();
        $('.list')
            .stop()
            .animate(
                {
                    left: -imgWidth * circleIndex + 'px',
                },
                1000
            );
        $('.circle').children().eq(prevCircleIndex).removeClass('current');
        $('.circle').children().eq(circleIndex).addClass('current');
        prevCircleIndex = circleIndex;
    });
    var flag = true;
    var timer;
    timer = setInterval(function () {
        if (flag) {
            $('.circle').children().eq(1).click();
            flag = false;
        } else {
            $('.circle').children().eq(0).click();
            flag = true;
        }
    }, 6000);
    $('.main').mouseenter(function () {
        clearInterval(timer);
        timer = null;
    });
    $('.main').mouseleave(function () {
        timer = setInterval(function () {
            if (flag) {
                $('.circle').children().eq(1).click();
                flag = false;
            } else {
                $('.circle').children().eq(0).click();
                flag = true;
            }
        }, 6000);
    });

    var aboutFiveH1Top = $('.aboutFive h1').offset().top;
    var aboutFiveAboutcTop = $('.aboutFive .aboutc').offset().top;
    var aboutTwoAboutTwoLeTop = $('.aboutTwo .aboutTwoLe').offset().top;
    var aboutTwoAboutTwoRiTop = $('.aboutTwo .aboutTwoRi').offset().top;
    var aboutThreeAboutTwoRiTop = $('.aboutThree .aboutTwoRi').offset().top;
    var aboutThreeAboutTwoLeTop = $('.aboutThree .aboutTwoLe').offset().top;
    var aboutForeAboutTwoLeTop = $('.aboutFore .aboutTwoLe').offset().top;
    var aboutForeAboutTwoRiTop = $('.aboutFore .aboutTwoRi').offset().top;
    var aboutSixAboutSixTopTop = $('.aboutSix .aboutSixTop').offset().top;
    var aboutSixAboutSixBotTop = $('.aboutSix .aboutSixBot').offset().top;
    var aboutSixQButton2Top = $('.aboutSix a.qButton2').offset().top;
    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
        var innerHeight = $(window).innerHeight() / 2;
        if (scrollTop > 300) {
            $('#backTop').css({
                display: 'block',
            });
        } else {
            $('#backTop').css({
                display: 'none',
            });
        }
        if (aboutFiveH1Top - 80 < scrollTop + innerHeight) {
            $('.aboutFive h1').animate({
                fontSize: 42 + 'px',
                opacity: 1,
            });
        }
        if (aboutFiveAboutcTop - 100 < scrollTop + innerHeight) {
            $('.aboutFive .aboutc').animate({
                top: 0 + 'px',
            });
        }
        if (aboutTwoAboutTwoLeTop - 100 < scrollTop + innerHeight) {
            $('.aboutTwo .aboutTwoLe').animate({
                left: 0 + 'px',
            });
        }
        if (aboutTwoAboutTwoRiTop - 100 < scrollTop + innerHeight) {
            $('.aboutTwo .aboutTwoRi').animate({
                left: 0 + 'px',
            });
        }
        if (aboutThreeAboutTwoRiTop - 100 < scrollTop + innerHeight) {
            $('.aboutThree .aboutTwoRi').animate({
                left: 0 + 'px',
            });
        }
        if (aboutThreeAboutTwoLeTop - 100 < scrollTop + innerHeight) {
            $('.aboutThree .aboutTwoLe').animate({
                left: 0 + 'px',
            });
        }
        if (aboutForeAboutTwoLeTop - 100 < scrollTop + innerHeight) {
            $('.aboutFore .aboutTwoLe').animate({
                left: 0 + 'px',
            });
        }
        if (aboutForeAboutTwoRiTop - 100 < scrollTop + innerHeight) {
            $('.aboutFore .aboutTwoRi').animate({
                left: 0 + 'px',
            });
        }
        if (aboutSixAboutSixTopTop - 150 < scrollTop + innerHeight) {
            $('.aboutSix .aboutSixTop').animate({
                fontSize: 32 + 'px',
                opacity: 1,
            });
        }
        if (aboutSixAboutSixBotTop - 150 < scrollTop + innerHeight) {
            $('.aboutSix .aboutSixBot .qhw1').animate({
                left: 0 + 'px',
            });
            $('.aboutSix .aboutSixBot .qhw2').animate({
                top: 0 + 'px',
            });
            $('.aboutSix .aboutSixBot .qhw3').animate({
                left: 0 + 'px',
            });
        }
        if (aboutSixQButton2Top - 200 < scrollTop + innerHeight) {
            $('.aboutSix a.qButton2').animate({
                opacity: 1,
            });
        }
    });
    $('#backTop').click(function () {
        var timer = setInterval(function () {
            var scrollTop = $(window).scrollTop();
            var speedTop = scrollTop / 5;
            if (scrollTop <= 0) {
                clearInterval(timer);
                scrollTop = 0;
            }
            $('html,body').scrollTop(scrollTop - speedTop);
        }, 20);
    });
});
