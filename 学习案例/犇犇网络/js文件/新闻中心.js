$(function () {
    var flag = true;
    function setinval() {
        if (flag) {
            flag = false;
            $('.bottom_sao .bottom_jia').css({
                transform: `rotateY(${60}deg)`,
            });
        } else {
            flag = true;
            $('.bottom_sao .bottom_jia').css({
                transform: `rotateY(${0}deg)`,
            });
        }
        return setinval;
    }
    setInterval(setinval(), 1500);
    $('#remove').click(function () {
        $(this).css({
            display: 'none',
        });
        $('#consult').animate(
            {
                width: 0 + '%',
            },
            1000
        );
        setTimeout(function () {
            $('#refer').css({
                display: 'block',
            });
        }, 1000);
    });
    $('#refer').click(function () {
        $('#refer').css({
            display: 'none',
        });
        $('#consult').animate(
            {
                width: 100 + '%',
            },
            1000
        );
        setTimeout(function () {
            $('#remove').css({
                display: 'block',
            });
        }, 1000);
    });
});
