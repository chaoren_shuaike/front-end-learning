$(function () {
    function animate1() {
        $('.huazhonghua_box').css({
            transform: `scale(1)`,
        });
        $('.huazhonghua_box2').css({
            opacity: 1,
            transform: `scale(1)`,
        });
        $('.img_hauzhonghua').css({
            top: -240 + 'px',
        });
        $('.huazhonghua_img').animate(
            {
                top: -415 + 'px',
            },
            2500
        );
        $('.textimg2').css({
            right: '0px',
        });
        $('.tetximg').css({
            top: '100px',
        });
        $('.textimg3').css({
            right: '50px',
        });
        $('.img_p').css({
            right: '188px',
        });
        $('.banner_img3').css({
            bottom: '0px',
        });
        $('.huazhonghua_box2').css({
            bottom: '82px',
        });
        $('.img_boxend').css({
            top: '42px',
        });
    }
    animate1();
    function reset1() {
        $('.huazhonghua_box').css({
            transform: `scale(0.1)`,
        });
        $('.huazhonghua_box2').css({
            opacity: 0,
            transform: `scale(0.1)`,
        });
        $('.img_hauzhonghua').css({
            top: 0 + 'px',
        });
        $('.huazhonghua_img').css({
            top: 0 + 'px',
        });
        $('.textimg2').css({
            right: '-521px',
        });
        $('.tetximg').css({
            top: '-76px',
        });
        $('.textimg3').css({
            right: '-440px',
        });
        $('.img_p').css({
            right: '-300px',
        });
        $('.banner_img3').css({
            bottom: '-280px',
        });
        $('.huazhonghua_box2').css({
            bottom: '-300px',
        });
        $('.img_boxend').css({
            top: '-86px',
        });
        $('.img_hauzhonghua').css({
            top: 0 + 'px',
        });
    }
    function animate2() {
        $('.banner2 .im4').animate(
            {
                left: 230 + 'px',
            },
            1200
        );
        $('.banner2 .im1').animate(
            {
                top: 88 + 'px',
            },
            1200
        );
        $('.banner2 .im2').animate(
            {
                right: 285 + 'px',
            },
            1200
        );
        $('.banner2 .im3').animate(
            {
                bottom: 100 + 'px',
            },
            1200
        );
        $('.banner2 .im8').css({
            top: 57 + 'px',
            transform: `rotate(0deg)`,
        });
        $('.banner2 .im5').css({
            top: 179 + 'px',
            transform: `rotate(0deg)`,
        });
        $('.banner2 .im7').css({
            left: 333 + 'px',
            transform: `rotate(0deg)`,
        });
        $('.banner2 .im6').css({
            left: 341 + 'px',
            transform: `rotate(0deg)`,
        });
    }
    function reset2() {
        $('.banner2 .im4').css({
            left: -550 + 'px',
        });
        $('.banner2 .im1').css({
            top: -300 + 'px',
        });
        $('.banner2 .im2').css({
            right: -300 + 'px',
        });
        $('.banner2 .im3').css({
            bottom: -200 + 'px',
        });
        $('.banner2 .im8').css({
            top: -100 + 'px',
            transform: `rotate(360deg)`,
        });
        $('.banner2 .im5').css({
            top: 450 + 'px',
            transform: `rotate(360deg)`,
        });
        $('.banner2 .im7').css({
            left: -200 + 'px',
            transform: `rotate(360deg)`,
        });
        $('.banner2 .im6').css({
            left: -200 + 'px',
            transform: `rotate(360deg)`,
        });
    }
    function animate3() {
        $('.banner3 .im1').css({
            transform: `scale(1)`,
        });
        $('.banner3 .im2').animate(
            {
                top: 240 + 'px',
            },
            1200
        );
        $('.banner3 .im3').animate(
            {
                top: 192 + 'px',
            },
            1200
        );
        $('.banner3 .im4').css({
            left: 250 + 'px',
            transform: `rotate(0deg)`,
        });
        $('.banner3 .p1 .sp1').css({
            left: 250 + 'px',
        });
        $('.banner3 .p1 .sp2').css({
            left: 370 + 'px',
        });
        $('.banner3 .p1 .sp3').css({
            left: 240 + 'px',
        });
        $('.banner3 .p2 .sp1').css({
            left: 350 + 'px',
        });
        $('.banner3 .p2 .sp2').css({
            left: 555 + 'px',
        });
        $('.banner3 .p2 .sp3').css({
            left: 310 + 'px',
        });
        $('.banner3 .p3 .sp1').css({
            left: 250 + 'px',
        });
        $('.banner3 .p3 .sp2').css({
            left: 250 + 'px',
        });
    }
    function reset3() {
        $('.banner3 .im1').css({
            transform: `scale(0.1)`,
        });
        $('.banner3 .im2').css({
            top: -400 + 'px',
        });
        $('.banner3 .im3').css({
            top: -400 + 'px',
        });
        $('.banner3 .im4').css({
            left: -200 + 'px',
            transform: `rotate(360deg)`,
        });
        $('.banner3 .p1 .sp1').css({
            left: -400 + 'px',
        });
        $('.banner3 .p1 .sp2').css({
            left: -400 + 'px',
        });
        $('.banner3 .p1 .sp3').css({
            left: -400 + 'px',
        });
        $('.banner3 .p2 .sp1').css({
            left: -600 + 'px',
        });
        $('.banner3 .p2 .sp2').css({
            left: -600 + 'px',
        });
        $('.banner3 .p2 .sp3').css({
            left: -600 + 'px',
        });
        $('.banner3 .p3 .sp1').css({
            left: -500 + 'px',
        });
        $('.banner3 .p3 .sp2').css({
            left: -500 + 'px',
        });
    }

    function animate4() {
        $('.banner4 .th1').animate(
            {
                top: 60 + 'px',
            },
            1000
        );
        $('.banner4 .th2').css({
            top: 80 + 'px',
        });
        $('.banner4 .th3').css({
            bottom: 265 + 'px',
        });
        $('.banner4 .im1').css({
            bottom: 225 + 'px',
        });
        $('.banner4 .im2').css({
            bottom: 225 + 'px',
        });
        $('.banner4 .im3').css({
            bottom: 0 + 'px',
        });
        $('.banner4 .im4').css({
            left: 888 + 'px',
        });
        $('.banner4 .im5').css({
            left: 777 + 'px',
        });
        $('.banner4 .im6').css({
            left: 666 + 'px',
        });
        $('.banner4 .im7').css({
            left: 500 + 'px',
        });
    }
    function reset4() {
        $('.banner4 .th1').css({
            top: -100 + 'px',
        });
        $('.banner4 .th2').css({
            top: -100 + 'px',
        });
        $('.banner4 .th3').css({
            bottom: 500 + 'px',
        });
        $('.banner4 .im1').css({
            bottom: 500 + 'px',
        });
        $('.banner4 .im2').css({
            bottom: 500 + 'px',
        });
        $('.banner4 .im3').css({
            bottom: -300 + 'px',
        });
        $('.banner4 .im4').css({
            left: -300 + 'px',
        });
        $('.banner4 .im5').css({
            left: -300 + 'px',
        });
        $('.banner4 .im6').css({
            left: -300 + 'px',
        });
        $('.banner4 .im7').css({
            left: -300 + 'px',
        });
    }
    var bans = $('.ban');
    var prevIndex = 0;
    var flag3 = true;
    $('.ban').click(function () {
        if (flag3) {
            flag3 = false;
            var index = $(this).index();
            $(bans[prevIndex]).css({
                display: 'none',
            });
            if (index + 1 == bans.length) {
                index = -1;
            }
            $(bans[index + 1]).css({
                display: 'block',
            });
            console.log(index);
            if (index == 0) {
                reset1();
                animate2();
            }
            if (index == 1) {
                reset2();
                animate3();
            }
            if (index == 2) {
                reset3();
                animate4();
            }
            if (index == -1) {
                reset4();
                animate1();
            }
            prevIndex = index + 1;
            setTimeout(function () {
                flag3 = true;
            }, 1200);
        }
    });

    var ulMarginTop = 0;
    $('.news .news_content_box .news_up').click(function () {
        ulMarginTop++;
        if (ulMarginTop == 2) {
            $('#news_txt_box').removeClass('tran');
            $('#news_txt_box').css({
                marginTop: -560 + 'px',
            });
            setTimeout(function () {
                $('#news_txt_box').addClass('tran');
                ulMarginTop = -1;
                $('#news_txt_box').css({
                    marginTop: -280 + ulMarginTop * 140 + 'px',
                });
            }, 16.7);
        } else {
            $('#news_txt_box').css({
                marginTop: -280 + ulMarginTop * 140 + 'px',
            });
        }
    });
    $('.news .news_content_box .news_down').click(function () {
        ulMarginTop--;
        if (ulMarginTop == -2) {
            $('#news_txt_box').removeClass('tran');
            $('#news_txt_box').css({
                marginTop: 0 + 'px',
            });
            setTimeout(function () {
                $('#news_txt_box').addClass('tran');
                ulMarginTop = 1;
                $('#news_txt_box').css({
                    marginTop: -280 + ulMarginTop * 140 + 'px',
                });
            }, 16.7);
        } else {
            $('#news_txt_box').css({
                marginTop: -280 + ulMarginTop * 140 + 'px',
            });
        }
    });
    var images = $('#cus-box img');
    var newsTop = $('.news').offset().top;
    var mapContTop = $('.map_cont').offset().top;
    var wordUsTop = $('.word .us').offset().top;
    var cusBoxTop = $('#cus-box').offset().top;
    var imgSrcArr = [
        '../images/1-151212151003I7.png',
        '../images/1-1505251010100-L.png',
        '../images/1-150525101041E1.png',
        '../images/1-1505251011520-L.png',
        '../images/1-1505251012270-L.png',

        '../images/1-1505251012460-L.png',
        '../images/1-1505251013030-L.png',
        '../images/1-1505251016090-L.png',
        '../images/1-1505251016260-L.png',
        '../images/1-1505251016450-L.png',

        '../images/1-1505251019210-L.png',
        '../images/1-1505251019370-L.png',
        '../images/1-1505251019510-L.png',
        '../images/1-150525101F40-L.png',
        '../images/1-150525101Z50-L.png',

        '../images/1-1505251020090-L.png',
        '../images/1-150525102032231.png',
        '../images/1-1505251021460-L.png',
        '../images/1-1505251022050-L.png',
        '../images/1-1505251022280-L.png',

        '../images/1-1505251022550-L.png',
        '../images/1-1505251024340-L.png',
        '../images/1-1511041F125T8.png',
        '../images/1-151212150045595.png',
        '../images/1-151212150AD53.png',
    ];
    $(window).scroll(function () {
        var innerHeight = $(window).innerHeight() / 2;
        var scrollTop = $(window).scrollTop();
        if (newsTop <= scrollTop + innerHeight) {
            $('#news_txt_box .one').animate(
                {
                    marginLeft: 0,
                },
                1000
            );
            $('#news_txt_box .two').animate(
                {
                    marginLeft: 0,
                },
                1000
            );
            $('#news_txt_box .three').animate(
                {
                    marginLeft: 0,
                },
                1000
            );
        }
        if (mapContTop + 200 <= scrollTop + innerHeight) {
            $('#map').css({
                top: 374 + 'px',
            });
            $('#nanyang').css({
                bottom: 483 + 'px',
            });
            $('.luoyang1').css({
                left: 670 + 'px',
            });
            $('.zhengzhou1').css({
                left: 875 + 'px',
            });
            $('#one').css({
                bottom: 180 + 'px',
            });
            $('#two').css({
                bottom: 215 + 'px',
            });
            $('#thi').css({
                bottom: 300 + 'px',
            });

            $('#fir').css({
                bottom: 180 + 'px',
            });
            $('#thir').css({
                bottom: 235 + 'px',
            });
            $('#fif').css({
                bottom: 301 + 'px',
            });
            $('#seven').css({
                bottom: 310 + 'px',
            });

            $('#jincheng').css({
                left: 310 + 'px',
            });
            $('#yangquan').css({
                left: 350 + 'px',
            });
            $('#shuozhou').css({
                left: 370 + 'px',
            });
            $('#yuncheng').css({
                left: 400 + 'px',
            });
            $('#lvliang').css({
                left: 410 + 'px',
            });
            $('#linfen').css({
                left: 445 + 'px',
            });
            $('#jinzhong').css({
                left: 465 + 'px',
            });
            $('#datong').css({
                left: 470 + 'px',
            });
            $('#changzhi').css({
                left: 515 + 'px',
            });

            $('#taiyuan').css({
                left: 600 + 'px',
            });
            $('#xinzhou').css({
                left: 600 + 'px',
            });
            $('#anshan').css({
                left: 680 + 'px',
            });
            $('#yingkou').css({
                left: 690 + 'px',
            });
            $('#fuxin').css({
                left: 705 + 'px',
            });
            $('#liaoyang').css({
                left: 720 + 'px',
            });
            $('#panjin').css({
                left: 765 + 'px',
            });
            $('#daqing').css({
                left: 780 + 'px',
            });
            $('#haerbin').css({
                left: 795 + 'px',
            });
            //
            $('#jiamusi').css({
                left: 860 + 'px',
            });
            $('#mudanjiang').css({
                left: 890 + 'px',
            });
            $('#hegang').css({
                left: 880 + 'px',
            });
            $('#heihe').css({
                left: 920 + 'px',
            });
            $('#shuangyashan').css({
                left: 910 + 'px',
            });
            $('#yichun').css({
                left: 945 + 'px',
            });
            $('#suihua').css({
                left: 960 + 'px',
            });
            $('#jixi').css({
                left: 950 + 'px',
            });
            $('#qiqihaer').css({
                left: 970 + 'px',
            });
        }
        if (wordUsTop + 70 <= scrollTop + innerHeight) {
            $('#contact .word .us img').animate({
                width: 406 + 'px',
                opacity: 1,
            });
            $('#contact .word .item').animate({
                fontSize: 18 + 'px',
                opacity: 1,
            });
        }

        var flag = true;
        if (cusBoxTop + 70 <= scrollTop + innerHeight) {
            if (flag) {
                flag = false;
                var i = 0;
                var flag2 = true;
                var timer = setInterval(function () {
                    if (i < 25) {
                        if (flag2) {
                            flag2 = false;
                            images[i].src = imgSrcArr[i];
                            i++;
                            flag2 = true;
                        }
                    } else {
                        clearInterval(timer);
                    }
                }, 120);
            }
        }
    });

    var flag = true;
    function setinval() {
        if (flag) {
            flag = false;
            $('.bottom_sao .bottom_jia').css({
                transform: `rotateY(${60}deg)`,
            });
        } else {
            flag = true;
            $('.bottom_sao .bottom_jia').css({
                transform: `rotateY(${0}deg)`,
            });
        }
        return setinval;
    }
    setInterval(setinval(), 1500);
    $('#remove').click(function () {
        $(this).css({
            display: 'none',
        });
        $('#consult').animate(
            {
                width: 0 + '%',
            },
            1000
        );
        setTimeout(function () {
            $('#refer').css({
                display: 'block',
            });
        }, 1000);
    });

    $('#refer').click(function () {
        $('#refer').css({
            display: 'none',
        });
        $('#consult').animate(
            {
                width: 100 + '%',
            },
            1000
        );
        setTimeout(function () {
            $('#remove').css({
                display: 'block',
            });
        }, 1000);
    });
});
